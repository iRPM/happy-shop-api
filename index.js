'use strict';


var kraken = require('kraken-js'),
  login = require('./lib/login/localStrategy'),
  passport = require('passport'),
  express = require('express'),
  logger = require('./lib/logger/bootstrap'),
  app = {};

app.configure = function configure(nconf, next) {
  // Async method run on startup.
  next(null);
};

app.requestStart = function requestStart(server) {
  // Run before most express middleware has been registered.
  server.use(passport.initialize());
  server.use(passport.session());
  server.use(express.bodyParser());
  server.use(express.cookieParser());
  server.use(express.session({secret: '1234567890QWERTY'}));
};

app.requestBeforeRoute = function requestBeforeRoute(server) {
  // Run before any routes have been added.
};

app.requestAfterRoute = function requestAfterRoute(server) {
  // Run after all routes have been added.
};

if (require.main === module) {
  kraken.create(app).listen(function (err) {
    if (err) {
      logger.info(err);
    }
  });
}

module.exports = app;