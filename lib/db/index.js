'use strict';

var mongoose = require('mongoose'),
  nconf = require('nconf'),
  connectionString = nconf.get('mongo:url'),
  options = {
    server: {
      auto_reconnect: true,
      poolSize: 10
    }
  };

mongoose.connection.open(connectionString, options);