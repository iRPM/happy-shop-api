'use strict';

var passport = require('passport'),
  LocalStrategy = require('passport-local').Strategy,
  Customer = require('../../models/customer');

passport.use(new LocalStrategy(Customer.login));