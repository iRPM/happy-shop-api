'use strict';

var nconf = require('nconf'),
  Logger = require('./bootstrap');

module.exports = new Logger(
  nconf.get('logger:filename'),
  nconf.get('logger:level')
);