'use strict';

var winston = require('winston');

function Logger(filename, level) {

  filename = filename || 'log/boot.log';
  level = level || 'silly';

  return winston.add(winston.transports.File, {
    filename: filename,
    maxsize: 1048576,
    maxFiles: 3,
    level: level
  });
}

module.exports = Logger;