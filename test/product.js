/*global describe:false, it:false*/
'use strict';

var app = require('../index')
  , kraken = require('kraken-js')
  , request = require('supertest')
  , nconf = require('nconf')
  , assert = require('assert')
  , mongoose = require('mongoose');

describe('product api', function () {

  var mock,
    productTest = {
      name: "GojiBerry",
      sku: 'testSku',
      description: 'blkflkdlkml kfkdf kfdlk d d gldfg kl f',
      url: "super-goji-berry",
      image: 'http://cdn.natue.com.br/BR/product/goji-berry-150g-giroil-11461-7725-16411-1-catalog.jpg',
      price: 49.80,
      priceOld: 69.90,
      active: true,
      created: Date.now,
      updated: Date.now
    };

  beforeEach(function (done) {

    mongoose.connection.collections['products'].drop(function (err) {
      mongoose.connection.collections['products'].insert(
        productTest,
        function(err){
          kraken.create(app).listen(function (err, server) {
            mock = server;
            done(err);
          });
        }
      );
    });
  });

  afterEach(function (done) {
    mock.close(done);
  });

  describe('when requesting existing product', function () {
    it('should respond with 200 and product', function (done) {
      request(mock)
        .get('/product/' + productTest.sku)
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function (err, res){
          var product = JSON.parse(res.text);
          assert.equal(product.sku, productTest.sku);
          assert.equal(product.name, productTest.name);
          assert.equal(product.description, productTest.description);
          assert.equal(product.url, productTest.url);
          assert.equal(product.image, productTest.image);
          assert.equal(product.price, productTest.price);
          assert.equal(product.priceOld, productTest.priceOld);
          assert.equal(product.active, productTest.active);
          done();
        });
    })
  })
});