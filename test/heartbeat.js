/*global describe:false, it:false*/

'use strict';

var app = require('../index')
  , kraken = require('kraken-js')
  , request = require('supertest');

describe('heartbeat api', function () {

  var mock;

  beforeEach(function (done) {
    kraken.create(app).listen(function (err, server) {
      mock = server;
      done(err);
    });
  });

  afterEach(function (done) {
    mock.close(done);
  });

  describe('when requesting resource / heartbeat', function () {
    it('should respond with 200', function (done) {
      request(mock)
        .get('/heartbeat')
        .expect('Content-Type', /json/)
        .expect(200, done);
    })
  })
});