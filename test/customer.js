/*global describe:false, it:false*/

'use strict';

var app = require('../index')
  , kraken = require('kraken-js')
  , request = require('supertest');

describe('customer api', function () {

  var mock,
    customerNew = {
      first_name: "Chuck",
      last_name: "Norris",
      email: "nobody@knows.what",
      password: "tooCoolForPassword"
    };

  beforeEach(function (done) {
    kraken.create(app).listen(function (err, server) {
      mock = server;
      done(err);
    });
  });

  afterEach(function (done) {
    mock.close(done);
  });

  describe('when requesting /customer when not logged in', function () {
    it('should respond with 401', function (done) {
      request(mock)
        .get('/customer')
        .expect('Content-Type', /json/)
        .expect(401, done);
    });
  });

  describe('when registering with unique email', function () {
    it('should respond with 200, also /customer',
      function (done) {
        var response = request(mock);

        response
          .post('/register')
          .send({
            first_name: customerNew.first_name,
            last_name: customerNew.first_name,
            email: 'nobody+' + Date.now() + '@knows.what',
            password: customerNew.password
          })
          .expect('Content-Type', /json/)
          .expect(200, function (err, res) {
            var req = request(mock).get('/customer');
            req.cookies = res.headers['set-cookie'].pop().split(';')[0];

            req
              .expect('Content-Type', /json/)
              .expect(200, done);
          });
      });
  });

  describe('when registering with existing email', function () {
    it('should respond second try with 409', function (done) {
      var response = request(mock);

      response
        .post('/register')
        .send({
          first_name: customerNew.first_name,
          last_name: customerNew.first_name,
          email: customerNew.email,
          password: customerNew.password
        })
        .expect('Content-Type', /json/)
        .expect(409, done);
    });
  });

});