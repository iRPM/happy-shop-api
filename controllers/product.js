'use strict';

var productModel = require('../models/product'),
  db = require('../lib/db');

module.exports = function (app) {

  app.get('/product/:sku', function (req, res) {

    productModel.get(req.params.sku, function (error, project) {
      if (error) {
        return res.json(500, error);
      }

      if (project === null) {
        return res.json(404, 'Not Found');
      }

      return res.json(200, project);
    });
  });
};