'use strict';

var customerModel = require('../models/customer'),
  logger = require('../lib/logger');

module.exports = function (app) {

  app.get('/customer', function (req, res) {

    if(req.session.customer){
      return res.json(200, req.session.customer);
    } else {
      return res.json(401, 'Unauthorized');
    }

  });

  app.post('/register', function (req, res) {

    customerModel.register(req.body, function(err, customer){
      if(err){
        if(err.name && err.name === 'ValidatorError'){
          return res.json(403, err);
        } else if(err.name && err.name === 'ConflictError'){
          return res.json(409, err);
        } else {
          return res.json(500, err);
        }
      }

      req.session.customer = customer;

      return res.json(200, customer);
    });
  });
};