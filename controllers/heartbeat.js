'use strict';

module.exports = function (app) {

  app.get('/heartbeat', function (req, res) {
    res.json(200, 'OK');
  });
};