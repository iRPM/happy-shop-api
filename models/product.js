'use strict';

var schemaProduct = require('./mongoose/product').model('Product'),
  _ = require('underscore');

function Product() {}

Product.prototype.get = function (sku, callback) {
  var query = {sku: sku};

  schemaProduct.findOne(query, function (error, response) {
    if (error) {
      return callback(error, null);
    }

    var product = _.pick(response, [
      'name',
      'sku',
      'description',
      'url',
      'image',
      'price',
      'priceOld',
      'active'
    ]);

    return callback(null, product);
  });
};

module.exports = new Product();