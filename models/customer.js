'use strict';

var schemaCustomer = require('./mongoose/customer').model('Customer'),
  _ = require('underscore'),
  check = require('validator').check;

function Customer() {
}

Customer.prototype.validPassword = function () {
  return false;
};

Customer.prototype.login = function (email, password, done) {

  schemaCustomer.findOne({ email: email }, function (err, user) {
    if (err) {
      return done(err);
    }
    if (!user) {
      return done(null, false, { message: 'Incorrect username.' });
    }
    if (!Customer.validPassword(password)) {
      return done(null, false, { message: 'Incorrect password.' });
    }
    return done(null, user);
  });
};

Customer.prototype.register = function (customer, done) {

  try {
    check(customer.first_name, 'Invalid first name').len(3, 64);
    check(customer.last_name, 'Invalid last name').len(3, 64);
    check(customer.email, 'Invalid email').len(6, 64).isEmail();
    check(customer.password, 'password').len(6, 64);
  } catch (err) {
    return done(err, null);
  }

  schemaCustomer.findOne({ email: customer.email}, function (err, customerDb) {
    if (err) {
      return done(err);
    }

    if (customerDb) {
      return done(
        {name: 'ConflictError', message: 'Email is already taken'},
        null
      );
    }

    schemaCustomer.create(customer, function (err, response) {

      if (err) {
        return done(err, null);
      }

      return done(null, response._doc);
    });

  });

};

module.exports = new Customer();