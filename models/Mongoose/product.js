'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var ProductSchema = new Schema({
  name: {
    type: String,
    required: true,
    index: true
  },
  sku: {
    type: String,
    required: true,
    index: true
  },
  description: {
    type: String
  },
  url: {
    type: String,
    required: true,
    index: true
  },
  image: {
    type: String
  },
  price: {
    type: Number,
    required: true
  },
  priceOld: {
    type: Number,
    required: true
  },
  active: {
    type: Boolean
  },
  created: {
    type: Date,
    default: Date.now
  },
  updated: {
    type: Date,
    default: Date.now
  }
});

mongoose.model('Product', ProductSchema);
module.exports = mongoose;